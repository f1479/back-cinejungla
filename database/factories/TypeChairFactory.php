<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use function PHPSTORM_META\type;

class TypeChairFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => $this->faker->text(5),
            'description'=>$this->faker->text(30),
            'price' => $this->faker->randomNumber(4,true)
        ];
    }
}
