<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'total' => $this->faker->randomNumber(5,true),
            'user_id' => $this->faker->numberBetween(1,30),
            'entertainment_id' => $this->faker->numberBetween(2,20),
            'chair_id' => $this->faker->numberBetween(1,40)
        ];
    }
}
