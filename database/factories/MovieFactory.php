<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'originTitle' => $this->faker->title(),
            'description' => $this->faker->text(30),
            'premiere' => $this->faker->date('Y-m-d'),
            'countryOrigin' => $this->faker->country(),
            'directors' => $this->faker->name(),
            'actors' => $this->faker->name(),
            'path' => Str::random(10),
            'img_banner' => $this->faker->url(),
            'img' => $this->faker->url(),
            'time' => $this->faker->numberBetween(60,180),
            'recommendation_id' => $this->faker->numberBetween(1,2),

        ];
    }
}
