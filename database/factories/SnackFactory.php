<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SnackFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'price' => $this->faker->randomNumber(6,true),
            'description' => $this->faker->text(30),
            'img' => $this->faker->url(),
            'type_snack_id'=>$this->faker->numberBetween(1,5)
        ];
    }
}
