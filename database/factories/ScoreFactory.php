<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ScoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'score' => $this->faker->numberBetween(0,5),
            'movie_id' => $this->faker->numberBetween(1,70),
            'user_id' => $this->faker->numberBetween(1,30)        
        ];
    }
}
