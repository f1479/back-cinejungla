<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EntertainmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hour' => $this->faker->time('H:i:s'),
            'date' => $this->faker->date('Y-m-d'),
            'movie_id' => $this->faker->numberBetween(1,70),
            'cinema_id' => $this->faker->numberBetween(1,7),
            'type_function_id' => $this->faker->numberBetween(1,4)
        ];
    }
}
