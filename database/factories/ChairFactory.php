<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ChairFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'number' => $this->faker->numberBetween(1,60),
            'row' => $this->faker->text(5),
            'type_chair_id' => $this->faker->numberBetween(1,2)
        ];
    }
}
