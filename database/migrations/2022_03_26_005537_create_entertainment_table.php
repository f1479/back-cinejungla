<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntertainmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entertainments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('date');
            $table->string('hour');
            $table->unsignedBigInteger('movie_id')->nullable();
            $table->unsignedBigInteger('cinema_id')->nullable();
            $table->unsignedBigInteger('type_function_id')->nullable();
            $table->foreign('movie_id')->references('id')->on('movies')->onDelete('set null');
            $table->foreign('cinema_id')->references('id')->on('cinemas')->onDelete('set null');
            $table->foreign('type_function_id')->references('id')->on('type_functions')->onDelete('set null');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entertainment');
    }
}
