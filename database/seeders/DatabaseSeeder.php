<?php

namespace Database\Seeders;

use App\Models\Chair;
use App\Models\Cinema;
use App\Models\Entertainment;
use App\Models\Genre;
use App\Models\Invoice;
use App\Models\Movie;
use App\Models\Recommendation;
use App\Models\Score;

use App\Models\Snack;
use App\Models\TypeChair;
use App\Models\TypeFunction;
use App\Models\TypeSnack;
use App\Models\User;
use FFI;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(10)->create();
        User::factory(30)->create();
        Cinema::factory(7)->create();
        TypeFunction::factory(4)->create();
        // TypeChair::factory(2)->create();
        TypeSnack::factory(5)->create();
        Recommendation::factory(10)->create();        
        Genre::factory(7)->hasAttached(
            Movie::factory()->count(10)
        )->create();
        Score::factory(20)->create();
        Entertainment::factory(20)->create();
        // Chair::factory(100)->create();
        Invoice::factory(30)->hasAttached(
            Snack::factory()->count(4)
        )->create();

    }
}
