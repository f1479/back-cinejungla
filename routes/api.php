<?php

use App\Http\Controllers\EntertainmentController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\MovieController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/food/{type?}',[FoodController::class,'ShowList'])->name('foot');
Route::get('/movies/{movie?}',[MovieController::class,'ShowList'])->name('movie');
Route::get('/functions/{pathMovie}/{date}',[EntertainmentController::class,'ShowList'])->name('function');
Route::get('/number-chairs/{functionId}', 'ChairController@numberOfChairs');
Route::get('/chairs-available/{typeChair}/{entertainmentId}','ChairController@chairsAvaible');
Route::get('/carousel-food','FoodController@carouselSnacks');
Route::post('/pay-purchase', BuyController::class);

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthCineController@login');
    Route::post('signup', 'AuthCineController@signUp');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthCineController@logout');
        Route::get('user', 'AuthCineController@user');
    });
});



