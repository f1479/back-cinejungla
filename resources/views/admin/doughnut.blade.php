<canvas id="doughnut" width="200" height="200"></canvas>
<script>
$(function () {
    var ctx = document.getElementById("doughnut").getContext('2d');
    var config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    @foreach($values as $value)
                        {{$value}},
                    @endforeach
                ],
                backgroundColor: [
                    @foreach($values as $value)
                        'rgb({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}})',
                    @endforeach
                ]
            }],
            labels: [
                @foreach($keys as $key)
                    "{{$key}}",
                @endforeach
            ]
        },
        options: {
            maintainAspectRatio: false
        }
    };

    var ctx = document.getElementById('doughnut').getContext('2d');
    new Chart(ctx, config);
});
</script>