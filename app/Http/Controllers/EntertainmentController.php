<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Models\Entertainment;
use App\Models\Movie;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class EntertainmentController extends Controller
{    
    /**
     * Method ShowList
     * 
     * This method return the movie available 
     * 
     * @param $pathMovie string name of movie 
     * @param $fecha date date of function
     *
     * @return JSON
     */
    public function ShowList($pathMovie, $fecha){
        return Cinema::whereHas('entertainments', function(Builder $query) use ($fecha, $pathMovie){
            $query->where('date',$fecha)->whereHas('movie', function(Builder $query)use ($pathMovie){
                $query->where('path',$pathMovie);
            });
        })->with(['entertainments' => function($query) use ($fecha){
            $query->where('date', $fecha);
        }])->get();
    }
}
