<?php

namespace App\Http\Controllers;

use App\Models\Snack;
use Illuminate\Http\Request;

class FoodController extends Controller
{    
    /**
     * Method ShowList
     *  return if exist snack selected in the cinema
     * 
     * @param $type text type of snack
     *
     * @return JSON
     */
    public function ShowList($type = null)
    {
    ($type) ? 
        $list = Snack::leftjoin('type_snacks','snacks.type_snack_id','=','type_snacks.id')->where('type_snacks.name',$type)->select('snacks.*','type_snacks.name AS type')->latest()->paginate(9)
    :
        $list = Snack::leftjoin('type_snacks','snacks.type_snack_id','=','type_snacks.id')->select('snacks.*','type_snacks.name AS type')->latest()->paginate(9);        
    return $list;
    }
    
    /**
     * Method carouselSnacks
     *  RETURN all snacks available in the cinema
     * @return JSOM
     */
    public function carouselSnacks(){
        return Snack::inRandomOrder()->limit(12)->get();
    }
}
