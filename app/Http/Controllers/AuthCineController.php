<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;

class AuthCineController extends Controller{
    
    /**
     * Method signUp
     * this method is intended to register on the web platform, 
     * first the parameters are validated, then the user is created 
     * if the validation is successful and ends up replying to the message with a 'success'
     * 
     * @param \Illuminate\Http\Request  $request ['name', 'email', 'gender', 'password']
     *
     * @return @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function signUp(Request $request){
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender,
            'password' => bcrypt($request->password)
        ]);

        return response()->json([
            'message' => 'success'
        ], 201);
    }
    
    /**
     * Method login
     * this method is intended to login on the web platform,
     * first the parameters are validate, then the user is authenticated,
     * and finally the method return a token
     *
     * @param \Illuminate\Http\Request  $request ['name', 'email', 'gender', 'password']
     *
     * @return @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function login(Request $request){
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'No autorizado'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }
    
    /**
     * Method logout
     * this method is intended to logout on the web platform, }
     * first the user is compare with the token request and 
     * finally delete the token in the server.
     * @param \Illuminate\Http\Request  $request ['name', 'email', 'gender', 'password']
     *
     * @return @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request){
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Usuario deslogueado'
        ]);
    }
    
    /**
     * Method user
     * The method is intended to display the user on the platform.
     * @param \Illuminate\Http\Request  $request ['name', 'email', 'gender', 'password']
     *
     * @return @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function user(Request $request){
        return response()->json($request->user());
    }


}
