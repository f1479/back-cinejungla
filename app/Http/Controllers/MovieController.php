<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function ShowList($movie = null )
    {
        $temp = Movie::with('genres:id,title as genres')
        ->leftjoin('recommendations','movies.recommendation_id'
        ,'recommendations.id')
        ->leftjoin('scores','movies.id','scores.id')
        ->select('movies.*',
        'recommendations.title as recommendations',
        'scores.score as total');
        $temp = ($movie) ? $temp->where('path', $movie)->first() : $temp->paginate(9);
        if($movie) return ['data' => $temp ? $temp : false];
        return $temp;
    }
}