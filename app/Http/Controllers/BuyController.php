<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\TypeChair;
use App\Models\Chair;
use App\Models\Snack;
use App\Models\Entertainment;
use Illuminate\Support\Facades\Mail;
use App\Mail\BuyTicket;

class BuyController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    /**
     * Handle the incoming request.
     * this method generates the purchase depending on the chair, site, snacks and more
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /*
        {
            function_id: num,
            type_chair: string,
            chairs: [{row: row, number: num},...], +10 points
            snacks: [id1, id2, id3, ...] +5points
        }
    */
    public function __invoke(Request $request)
    {
        try {
            $user = $request->user();
            $priceChair = TypeChair::where('type',$request->type_chair)->first()->price;
            $totalPriceChair = count($request->chairs)*$priceChair;
            $totalPriceSnack = 0;
            foreach ($request->snacks as $key => $snack) {
                $totalPriceSnack += Snack::find($snack)->price;
            }
    
            if($request->type_chair == 'general' && $user->points >= 100){
                //points --------------
                if(count($request->chairs) >= floor($user->points/100)){
                    $totalPriceChair =  $totalPriceChair - ($priceChair*floor($user->points/100));
                    $user->points = ($user->points-100*floor($user->points/100))+(10*(count($request->chairs)-floor($user->points/100)))+(5*count($request->snacks));
                }else{
                    $totalPriceChair = 0;
                    $user->points  = $user->points-100*count($request->chairs)+(5*count($request->snacks));
                }
                $user->save();
                //--------------
                foreach ($request->chairs as $key => $chair){
                    Invoice::create([
                        'total' => $totalPriceChair + $totalPriceSnack,
                        'user_id' => $user->id,
                        'entertainment_id' => $request->function_id,
                        'chair_id' => Chair::where('row', $chair['row'])->where('number', $chair['number'])->first()->id
                    ]);
                }
            }else{
                $user->points = $user->points + (10*count($request->chairs)) + (5*count($request->snacks));
                $user->save();
                foreach ($request->chairs as $key => $chair){
                    Invoice::create([
                        'total' => $totalPriceChair + $totalPriceSnack,
                        'user_id' => $user->id,
                        'entertainment_id' => $request->function_id,
                        'chair_id' => Chair::where('row', $chair['row'])->where('number', $chair['number'])->first()->id
                    ]);
                }
            }
            $this->viewsAdd($request);
            return $this->sendEmail($request, $totalPriceChair + $totalPriceSnack);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th 
            ], 500);
        }
    }
    
    
    /**
     * Method sendEmail
     * This method creates an email where the information about the purchase is sent to the user.
     * @param $data JSON [title movie, image, name from cinema, address, date and hour]
     * @param $totalPay textNumber number of total to pay
     *
     * @return void
     */
    public function sendEmail($data, $totalPay){

        $entertainment = Entertainment::find($data->function_id);
        $chairs = [];
        foreach ($data->chairs as $key => $chair) {
            array_push($chairs, $chair['row'].$chair['number']);
        }
        $infoMail = [
            "user_name" => $data->user()->name,
            "movie" => $entertainment->movie->title,
            "movie_image" => $entertainment->movie->img,
            "cinema_name" => $entertainment->cinema->name,
            "cinema_address" => $entertainment->cinema->address,
            "schedule" => $entertainment->date." a las ".$entertainment->hour,
            "chairs" => $chairs,
            "snacks" => Snack::whereIn('id', $data->snacks)->pluck('name'),
            "total_pay" => $totalPay
        ];
        return Mail::to($data->user()->email)->send(new BuyTicket($infoMail));
    }
    
    /**
     * Method viewsAdd
     *  this method looks for the available movie function and will finally return if there are snacks
     * @param $data JSON [Function id, snacks to buy]
     *
     * @return text
     */
    public function viewsAdd($data){
        $cinema = Entertainment::find($data->function_id)->cinema;
        $cinema->views += 1;
        $cinema->save();
        $snacks = Snack::whereIn('id', $data->snacks)->get();
        foreach ($snacks as $key => $snack) {
           $snack->views += 1;
           $snack->save();
        }
        return 'ok';
    }
}
