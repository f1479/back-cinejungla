<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chair;
use App\Models\Invoice;
use App\Models\TypeChair;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use stdClass;

class ChairController extends Controller
{
    
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth:api');
    }
    
    /**
     * Method numberOfChairs
     * this method return the number of chairs
     * 
     * @param $functionId JSON [function, type chair]
     *
     * @return JSON [numbers of chairs, general price, type chair, preferencial chair price]
     */
    public function numberOfChairs($functionId){
        $general = TypeChair::where('type', 'general')->first();
        $preferencial = TypeChair::where('type', 'preferencial')->first();
        $chairs = Chair::whereDoesntHave('invoices', function(Builder $query) use ($functionId){
            $query->where('entertainment_id', $functionId);
        })->get();
        return [
            "general" => count($chairs->where('type_chair_id', $general->id)),
            "general_price" => $general->price,
            "preferencial" => count($chairs->where('type_chair_id', $preferencial->id)),
            "preferencial_price" => $preferencial->price,
        ];
    }

        
    /**
     * Method chairsAvaible
     *  this method return if the chairs are avaible
     * @param $typeChair string type chair
     * @param $entertainmentId number number of function
     *
     * @return void
     */
    public function chairsAvaible( $typeChair, $entertainmentId)
    {   
        $getTypeChair = TypeChair::where('type', $typeChair)->first();
        return Chair::whereDoesntHave('invoices', function(Builder $query) use ($entertainmentId){
            $query->where('entertainment_id', $entertainmentId);
        })->where('type_chair_id', $getTypeChair->id)->select('row','number')->get()->groupBy('row');
    }
}
