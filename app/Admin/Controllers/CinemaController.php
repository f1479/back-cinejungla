<?php

namespace App\Admin\Controllers;

use App\Models\Cinema;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;

class CinemaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Cinemas';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Cinema());
        
        $grid->header(function ($query) {

            $cinemas = $query->orderBy('views','desc')->limit(8)->pluck('views', 'name')->toArray();
            $values = array_values($cinemas);
            $keys = array_keys($cinemas);
            $doughnut = view('admin.doughnut', compact('values','keys'));
        
            return new Box('Cinemas mas visitados', $doughnut);
        });

        $grid->column('id', __('Id'));
        $grid->column('name', __('Nombre'));
        $grid->column('address', __('Dirección'));
        $grid->column('phone', __('Teléfono'));
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('name', 'Nombre de cinema');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Cinema::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Nombre'));
        $show->field('address', __('Dirección'));
        $show->field('phone', __('Teléfono'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Cinema());

        $form->text('name', __('Nombre'))->placeholder('Nombre de cinema');
        $form->text('address', __('Dirección'))->placeholder('Dirección');
        $form->mobile('phone', __('Teléfono'))->options(['mask' => '9999999999'])->placeholder('Teléfono');

        return $form;
    }
}
