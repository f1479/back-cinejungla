<?php

namespace App\Admin\Controllers;

use App\Models\Entertainment;
use App\Models\Cinema;
use App\Models\TypeFunction;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Selectable\Movies;

class EntertainmentController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Funciones';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Entertainment());
        $grid->model()->orderBy('id', 'desc');
        $grid->column('id', __('Id'));
        $grid->column('date', __('Fecha'));
        $grid->column('hour', __('Hora'));
        $grid->column('movie.title', __('Película'));
        $grid->column('cinema.name', __('Cinema'));
        $grid->column('type_function.title', __('Tipo de función'));
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('movie.title', 'Película');
            $filter->like('cinema.name', 'Cinema');
            $filter->like('type_function.title', 'Tipo de función');
            $filter->between('date', 'Fecha')->datetime();

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Entertainment::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('date', __('Fecha'));
        $show->field('hour', __('Hora'));
        $show->field('movie_id', __('Película'));
        $show->field('cinema_id', __('Cinema'));
        $show->field('type_function_id', __('Tipo de función'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Entertainment());

        $form->date('date', __('Fecha'))->format('YYYY-MM-DD')->placeholder('Fecha');
        $form->datetime('hour', __('Hora'))->format('HH:mm')->placeholder('Hora');
        $form->belongsTo('movie_id', Movies::class ,__('Película'));
        $form->select('cinema_id', __('Cinema'))->options(Cinema::pluck('name', 'id'))->placeholder('Cinema');
        $form->select('type_function_id', __('Tipo de Función'))
                ->options(TypeFunction::pluck('title', 'id'))->placeholder('Tipo de Función');

        return $form;
    }
}
