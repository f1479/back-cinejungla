<?php

namespace App\Admin\Controllers;

use App\Models\Snack;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use App\Models\TypeSnack;

class SnackController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Producto';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Snack());
        
        $grid->header(function ($query) {

            $snacks = $query->orderBy('views','desc')->limit(8)->pluck('views', 'name')->toArray();
            $values = array_values($snacks);
            $keys = array_keys($snacks);
            $doughnut = view('admin.doughnut', compact('values','keys'));
        
            return new Box('Snacks mas comprados', $doughnut);
        });

        $grid->column('id', __('Id'));
        $grid->column('name', __('Nombre'));
        $grid->column('price', __('Precio'))->display(function ($price){
            return (!$price || $price == '') ? "$0" : "$".$price; 
        });
        $grid->column('img', __('Imagen'))->image();
        $grid->column('type_snack.name', __('Tipo de Snack'))->label('success');
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('name', 'Nombre');
            $filter->like('type_snack.name', 'Tipo de Snack');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Snack::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Nombre'));
        $show->field('price', __('Precio'));
        $show->field('description', __('Descripción'));
        $show->field('img', __('Ruta imagen'));
        $show->field('type_snacks.name_', __('Tipo de snack'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));
        
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Snack());

        $form->text('name', __('Nombre'))->placeholder('Nombre de producto');
        $form->number('price', __('Precio'))->min(0)->placeholder('Precio');
        $form->ckeditor('description', __('Descripción'))->placeholder('Descripción');
        $form->image('img', __('Imagen'))->removable()->uniqueName();
        $form->select('type_snack_id', __('Tipo de Snack'))->options(TypeSnack::pluck('name', 'id'))->placeholder('Tipo de snack');

        return $form;
    }
}
