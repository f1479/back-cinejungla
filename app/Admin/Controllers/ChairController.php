<?php

namespace App\Admin\Controllers;

use App\Models\Chair;
use App\Models\TypeChair;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ChairController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Silla';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Chair());

        $grid->column('id', __('Id'));
        $grid->column('number', __('Número'));
        $grid->column('row', __('Fila'));
        $grid->column('type_chair.type', __('Tipo de Silla'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Chair::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('number', __('Número'));
        $show->field('row', __('Fila'));
        $show->field('type_chair_id', __('Tipo de Silla'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Chair());

        $form->number('number', __('Número'))->min(1)->max(10)->placeholder('Número');
        $form->text('row', __('Fila'))->placeholder('Fila');
        $form->select('type_chair_id', __('Tipo de Silla'))
                ->options(TypeChair::pluck('type', 'id'))->placeholder('Tipo de Silla');

        return $form;
    }
}
