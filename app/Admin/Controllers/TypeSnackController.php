<?php

namespace App\Admin\Controllers;

use App\Models\TypeSnack;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TypeSnackController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tipo de Snack';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TypeSnack());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Nombre'));
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', 'Nombre');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TypeSnack::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Nombre'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TypeSnack());

        $form->text('name', __('Nombre'))->placeholder('Tipo de snack');

        return $form;
    }
}
