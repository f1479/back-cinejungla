<?php

namespace App\Admin\Controllers;

use App\Models\Movie;
use App\Models\Recommendation;
use App\Models\Genre;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MovieController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Películas';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Movie());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Título'));
        $grid->column('originTitle', __('Título Original'));
        $grid->column('premiere', __('Estreno'))->date('Y-m-d');;
        $grid->column('img', __('Imagen'))->image();
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', 'Título');
            $filter->like('originTitle', 'Título Original');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Movie::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Título'));
        $show->field('originTitle', __('Título Original'));
        $show->field('description', __('Descripción'));
        $show->field('premiere', __('Estreno'));
        $show->field('countryOrigin', __('País de Origen'));
        $show->field('directors', __('Directores'));
        $show->field('actors', __('Actores'));
        $show->field('path', __('Path'));
        $show->field('img', __('Ruta Imagen'));
        $show->field('img_banner', __('Ruta Banner'));
        $show->field('recommendation_id', __('Recomendación'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Movie());
        $genres = Genre::pluck('title', 'id');

        $form->text('title', __('Título'))->placeholder('Título');
        $form->text('originTitle', __('Título Original'))->placeholder('Título Original');
        $form->ckeditor('description', __('Descripción'));
        $form->date('premiere', __('Estreno'))->format('YYYY-MM-DD')->placeholder('Estreno');
        $form->text('countryOrigin', __('País de Origen'))->placeholder('País de Origen');
        $form->text('directors', __('Directores'))->placeholder('Directores');
        $form->text('actors', __('Actores'))->placeholder('Actores');
        $form->text('path', __('Path'))->placeholder('Ruta')
        ->help('Coloca solo nombre de la ruta como aparecera la pelicula, ej: /el-destierro');
        $form->number('time', __('Tiempo'))->placeholder('Tiempo');
        $form->image('img', __('Imagen'))->removable()->uniqueName();
        $form->image('img_banner', __('Imagen para banner'))->removable()->uniqueName();
        $form->select('recommendation_id', __('Recomendación'))
                ->options(Recommendation::pluck('title', 'id'))->placeholder('Tipo de Recomendación');
        $form->multipleSelect('genres', __('Generos'))->options($genres);

        return $form;
    }
}
