<?php

namespace App\Admin\Controllers;

use App\Models\Recommendation;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RecommendationController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Recomendaciones';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Recommendation());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Título'));
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', 'Título');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Recommendation::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Título'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Recommendation());

        $form->text('title', __('Título'))->placeholder('Tipo de Recomendación');

        return $form;
    }
}
