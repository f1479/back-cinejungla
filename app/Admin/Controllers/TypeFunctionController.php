<?php

namespace App\Admin\Controllers;

use App\Models\TypeFunction;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TypeFunctionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tipo de Función';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TypeFunction());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Tipo de Función'));
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', 'Tipo de Función');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TypeFunction::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Tipo de Función'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TypeFunction());

        $form->text('title', __('Tipo de Función'))->placeholder('Tipo de función');

        return $form;
    }
}
