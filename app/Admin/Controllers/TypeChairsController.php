<?php

namespace App\Admin\Controllers;

use App\Models\TypeChair;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TypeChairsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tipo de Silla';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TypeChair());

        $grid->column('id', __('Id'));
        $grid->column('type', __('Tipo'));
        $grid->column('price', __('Precio'))->display(function ($price){
            return (!$price || $price == '') ? "$0" : "$".$price; 
        });
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('type', 'Tipo');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TypeChair::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('type', __('Tipo'));
        $show->field('description', __('Descripción'));
        $show->field('price', __('Precio'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TypeChair());

        $form->text('type', __('Nombre'))->placeholder('Tipo de silla');
        $form->ckeditor('description', __('Description'))->placeholder('Descripción');
        $form->number('price', __('Price'))->placeholder('Precio');

        return $form;
    }
}
