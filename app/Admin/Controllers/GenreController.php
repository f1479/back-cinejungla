<?php

namespace App\Admin\Controllers;

use App\Models\Genre;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class GenreController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Generos';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Genre());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Nombre'));
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', 'Nombre');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Genre::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Nombre'));
        $show->field('created_at', __('Creado'));
        $show->field('updated_at', __('Actualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Genre());

        $form->text('title', __('Nombre'))->placeholder('Nombre de género');

        return $form;
    }
}
