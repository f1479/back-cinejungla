<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    //Snacks
    $router->resource('/type-snacks', TypeSnackController::class);
    $router->resource('/snacks', SnackController::class);
    //Functions
    $router->resource('/type-function', TypeFunctionController::class);
    $router->resource('/cinemas', CinemaController::class);
    $router->resource('/functions', EntertainmentController::class);
    //Movies
    $router->resource('/genres', GenreController::class);
    $router->resource('/recommendations', RecommendationController::class);
    $router->resource('/movies', MovieController::class);
    //Chairs
    $router->resource('/type-chairs', TypeChairsController::class);
    $router->resource('/chairs', ChairController::class);

});
