<?php

namespace App\Admin\Selectable;

use App\Models\Movie;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Grid\Selectable;

class Movies extends Selectable
{
    public $model = Movie::class;

    public function make()
    {
        $this->column('id');
        $this->column('title', 'Título');
        $this->column('premiere', 'Estreno')->date('Y-m-d');;
        $this->column('img','Imagen')->image();

        $this->filter(function (Filter $filter) {
            $filter->disableIdFilter();
            $filter->like('title');
        });
    }
}