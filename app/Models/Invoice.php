<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = ['total','user_id','entertainment_id', 'chair_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function entertainment()
    {
        return $this->belongsTo(Entertainment::class);
    }
    public function chair()
    {
        return $this->belongsTo(Chair::class);
    }
    public function snacks(){
        return $this->belongsToMany(Snack::class)->withTimestamps();;
    }    
}
