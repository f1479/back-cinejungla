<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeChair extends Model
{
    use HasFactory;
    protected $fillenable = ['type', 'description', 'price'];

    public function chairs()
    {
        return $this->hasMany(Chair::class);
    }
}
