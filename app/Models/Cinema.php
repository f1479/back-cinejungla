<?php

namespace App\Models;

use App\Admin\Selectable\Movies;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    use HasFactory;

    protected $fillable = ['name','address','phone'];

    public function entertainments(){
        return $this->hasMany(Entertainment::class);
    }
    public function moviesInCinema(){
        return $this->hasManyThrough(Movies::class, Entertainment::class);
    }
}
