<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeSnack extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function snacks()
    {
        return $this->hasMany(Snack::class);
    }    
}
