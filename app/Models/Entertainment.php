<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entertainment extends Model
{
    use HasFactory;

    protected $table = 'entertainments';

    protected $fillable = ['date', 'hour', 'movie_id', 'cinema_id', 'type_function_id'];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function type_function()
    {
        return $this->belongsTo(TypeFunction::class);
    }

    public function cinema()
    {
        return $this->belongsTo(Cinema::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }    
}
