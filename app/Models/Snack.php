<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Snack extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'price', 'description', 'type_snack_id'];

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class);
    }

    public function type_snack()
    {
        return $this->belongsTo(TypeSnack::class);
    }

    //MUTATIONS
    public function getImgAttribute($value){
        return $value ? env('APP_STORAGE').$value : $value;
    }
}
