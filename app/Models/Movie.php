<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'originTitle', 'description',
                            'premiere', 'countryOrigin', 'directors',
                            'actors','path'];

    public function recommendation(){
        return $this->belongsTo(Recommendation::class);
    }
    public function genres(){
        return $this->belongsToMany(Genre::class);
    }
    public function scores(){
        return $this->hasMany(Score::class);
    }
    public function entertainments(){
        return $this->hasMany(Entertainment::class);
    }

    //MUTATIONS
    public function getImgAttribute($value){
        return $value ? env('APP_STORAGE').$value : $value;
    }
    public function getImgBannerAttribute($value){
        return $value ? env('APP_STORAGE').$value : $value;
    }
}
