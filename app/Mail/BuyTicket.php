<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BuyTicket extends Mailable
{
    use Queueable, SerializesModels;

    public $infoPrint;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($infoPrint)
    {
        $this->infoPrint = $infoPrint;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pelicula Reservada desde Cine Jungla')->view('buy-ticket');
    }
}
